import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailScreen extends StatefulWidget {
  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    final sWidth = MediaQuery.of(context).size.width;
    final sHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: sWidth,
              height: 600,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage('assets/bg.png'))),
              child: Image.asset(
                'assets/guitar_man.png',
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Positioned(
            top: 30,
            child: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.arrow_back, size: 30,),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: sWidth,
              height: 500,
              decoration: BoxDecoration(
                  color: Color(0xffecf0f8),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                  image: DecorationImage(
                      image: AssetImage("assets/mask_by_mask.png"),
                      fit: BoxFit.cover)),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Guitar',
                      style: GoogleFonts.montserrat(
                          color: Color(0xff41416a),
                          fontSize: 28,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 30,),
                    Text(
                      'Available Courses',
                      style: GoogleFonts.montserrat(
                          color: Color(0xff41416a), fontSize: 20),
                    ),
                    SizedBox(height: 10,),
                    buildItem('1 Month for basics', '12 lessons', false),
                    buildItem('2 Months course', '30 lessons', true),
                    buildItem('3 Months course', '48 lessons', false),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Color(0xff314dcb),
                            borderRadius: BorderRadius.circular(30)
                          ),
                          width: 250,
                          height: 60,
                          child: Center(
                            child: Text('Join', style: GoogleFonts.montserrat(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white),),
                          ),
                        ),
                      ],
                    )

                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildItem(String title, String subtitle, bool check) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
        child: ListTile(
          title: Text(
            title,
            style: GoogleFonts.montserrat(fontSize: 20, color: Color(0xff41416a)),
          ),
          subtitle: Text(
            subtitle,
            style: GoogleFonts.montserrat(fontSize: 18, color: Color(0xff41416a)),
          ),
          trailing: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: check ? Color(0xff30c4ae) : Color(0xffbbe8d9)),
            child: Center(
              child: check
                  ? Icon(
                      Icons.check,
                      size: 30,
                      color: Colors.white,
                    )
                  : Text(''),
            ),
          ),
        ),
      ),
    );
  }
}
