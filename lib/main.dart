import 'package:app/detail.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final sWidth = MediaQuery.of(context).size.width;
    final sHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 30),
                      width: sWidth,
                      height: 350,
                      decoration: BoxDecoration(
                          color: Color(0xff314dcb),
                          image: DecorationImage(
                              image: AssetImage('assets/group.png'))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 250,
                            child: Text(
                              'Find the best music tutorial',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            width: 80,
                            height: 80,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage('assets/profile.png'),
                                    fit: BoxFit.cover)),
                          )
                        ],
                      )),
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    width: sWidth,
                    child: Image.asset('assets/page.png'),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Container(
                      padding: const EdgeInsets.only(left: 20),
                      width: sWidth - 40,
                      height: 60,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border:
                          Border.all(color: Color(0xff7f91b9), width: 2)),
                      child: Center(
                        child: TextField(
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: Color(0xff7f91b9), fontSize: 24),
                              hintText: 'Search',
                              suffixIcon: Icon(
                                Icons.search,
                                color: Color(0xff7f91b9),
                                size: 30,
                              )),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20, right: 20, top: 20, bottom: 20),
                    child: Text(
                      'Popular tutorial',
                      style: TextStyle(
                          color: Color(0xff41416a),
                          fontWeight: FontWeight.bold,
                          fontSize: 25),
                    ),
                  ),
                  Divider(),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      children: <Widget>[
                        buildItem('Basic yutorial of Guitar', 'Marty Schwartz Guitar', true),
                        Divider(),
                        buildItem('Notes on Violin(First', 'Measure By Measure', false),
                        Divider(),
                        buildItem('Guitar Lesson - How To', 'Marty Schwartz Guitar', false),
                      ],
                    ),
                  )
                ],
              ),
              Positioned(
                top: 100,
                child: Container(
                  width: sWidth,
                  height: 300,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: 20,),
                        GestureDetector(
                          onTap: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => DetailScreen())),
                          child: Stack(
                            children: <Widget>[
                              Container(
                                width: 150,
                                height: 260,
                              ),

                              Positioned(
                                bottom: 0,
                                child: Container(
                                  width: 150,
                                  height: 60,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(30),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(.2),
                                            spreadRadius: 5,
                                            blurRadius: 10,
                                            offset: Offset(0, 5)
                                        )
                                      ]
                                  ),
                                  child: Center(child: Text('Saxophone', style: GoogleFonts.montserrat(color: Color(0xff7f91b9).withOpacity(.7), fontSize: 17, fontWeight: FontWeight.w600),),),
                                ),
                              ),
                              Positioned(
                                bottom: 60,
                                child: Container(
                                  height: 200,
                                  width: 150,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage('assets/saxophone.png'),
                                          fit: BoxFit.fitHeight
                                      )
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 70,),
                        Stack(
                          children: <Widget>[
                            Container(
                              width: 150,
                              height: 260,
                            ),

                            Positioned(
                              bottom: 0,
                              child: Container(
                                  width: 150,
                                  height: 60,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(30),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(.2),
                                            spreadRadius: 5,
                                            blurRadius: 10,
                                            offset: Offset(0, 5)
                                        )
                                      ]
                                  ),
                                  child: Center(child: Text('Violin', style: GoogleFonts.montserrat(color: Color(0xff7f91b9).withOpacity(.7), fontSize: 17, fontWeight: FontWeight.w600),))
                              ),
                            ),
                            Positioned(
                              bottom: 50,
                              child: Container(
                                height: 200,
                                width: 150,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('assets/violin.png'),
                                        fit: BoxFit.fitHeight
                                    )
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(width: 70,),
                        Stack(
                          children: <Widget>[
                            Container(
                              width: 150,
                              height: 260,
                            ),

                            Positioned(
                              bottom: 0,
                              child: Container(
                                width: 150,
                                height: 60,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey.withOpacity(.2),
                                          spreadRadius: 5,
                                          blurRadius: 10,
                                          offset: Offset(0, 5)
                                      )
                                    ]
                                ),
                                child: Center(child: Text('Guitar', style: GoogleFonts.montserrat(color: Color(0xff7f91b9).withOpacity(.7), fontSize: 17, fontWeight: FontWeight.w600),),),
                              ),
                            ),
                            Positioned(
                              bottom: 50,
                              child: Container(
                                height: 200,
                                width: 150,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('assets/guitar.png'),
                                        fit: BoxFit.fitHeight
                                    )
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(width: 20,),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildItem(String title, String subtitle, bool check) {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      leading: Container(
        width: 60,
        height: 60,
        decoration: BoxDecoration(
            border: Border.all(color: Color(0xffff3a70), width: 1),
            color: check ? Color(0xffff3a70) : Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              check ? BoxShadow(
                  color: Color(0xffff3a70).withOpacity(.1),
                  blurRadius: 10,
                  spreadRadius: 5,
                  offset: Offset(0, 5)
              ) : BoxShadow()
            ]
        ),
        child: Icon(
          Icons.play_arrow,
          color: check ? Colors.white : Color(0xffff3a70),
        ),
      ),
      title: Text(title, style: GoogleFonts.montserrat(color: Color(0xff41416a), fontSize: 22),),
      subtitle: Text(subtitle,style: GoogleFonts.montserrat(color: Color(0xff6c6c70), fontSize: 18),),
    );
  }

}
